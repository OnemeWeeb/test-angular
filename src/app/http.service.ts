import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { URL } from '../common/constants'
  
@Injectable()
export class HttpService{
  
    constructor(private http: HttpClient){ }
      
    getData(){
        return this.http.get(URL);
    }
}