import { Input, Output, Component, EventEmitter } from '@angular/core';
import { User } from '../user';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})

export class userTableComponent {
	@Input() userList: User[]=[];

	@Output() handleClick = new EventEmitter<string>();

	click(name: string) { // Column header click handler
		this.handleClick.emit( name );
	}
}