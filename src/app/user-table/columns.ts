export interface Columns {
	name: string,
	age: string,
	gender: string,
	department: string,
	address: string,
}