import { Component, OnInit, DoCheck } from '@angular/core';
import { HttpService } from './http.service';
import { User } from './user';
import { Filters } from './filter-panel/filters';
import { Columns } from './user-table/columns'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
	providers: [HttpService]
})

export class AppComponent implements OnInit, DoCheck {
	users: User[] = [];
	filtredUsers: User[] = [];
	filters: Filters = {
		gender: '',
		department: '',
		city: '',
	};
	columns: Columns = {
		name: '',
		age: '',
		gender: '',
		department: '',
		address: '',
	}

	constructor(private httpService: HttpService) { }

	ngOnInit() { // Getting a list of users
		this.httpService.getData().subscribe((data: []) => {
			this.users = data;
			this.filtredUsers = this.users;
		});
	}

	handleChange(filter: { name: string, value: string }) { // Getting the active filter value
		this.filters[filter.name] = filter.value;
	}

	sortColumn(name: string) { // Sort Column Descending / Ascending
		this.columns[name] = this.columns[name]
			? (this.columns[name] === 'increase')
				? 'decrease'
				: 'increase'
			: 'increase';

		this.users = this.users.sort((prev, next) => {
			if (name === 'address') {
				if ((prev.address.city + prev.address.street) > (next.address.city + next.address.street)) {
					return this.columns.address === 'increase' ? 1 : -1;
				} else if ((prev.address.city + prev.address.street) < (next.address.city + next.address.street)) {
					return this.columns.address === 'increase' ? -1 : 1;
				}
			} else {
				if (prev[name] > next[name]) {
					return this.columns[name] === 'increase' ? 1 : -1;
				} else if (prev[name] < next[name]) {
					return this.columns[name] === 'increase' ? -1 : 1;
				}
			}
		});
	}

	ngDoCheck() { // Applying filters on the user list
		this.filtredUsers = this.users
			.filter(user => this.filters.gender ? this.filters.gender === user.gender : user)
			.filter(user => this.filters.department ? this.filters.department === user.department : user)
			.filter(user => this.filters.city ? this.filters.city === user.address.city : user);
	}
}