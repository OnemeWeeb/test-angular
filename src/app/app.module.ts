import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule }   from '@angular/common/http';

import { AppComponent } from './app.component';
import { userTableComponent } from './user-table/user-table.component';
import { filterPanelComponent } from './filter-panel/filter-panel.component';

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [
    AppComponent,
    userTableComponent,
    filterPanelComponent
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
