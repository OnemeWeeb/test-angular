import { Input, Output, Component, EventEmitter, OnChanges } from '@angular/core';
import { User } from '../user';

@Component({
	selector: 'app-filter-panel',
	templateUrl: './filter-panel.component.html',
	styleUrls: ['./filter-panel.component.css']
})

export class filterPanelComponent implements OnChanges {
	@Input() userList: User[] = [];

	genderList: string[] = [];
	departmentList: string[] = [];
	cityList: string[] = [];
	genderCount: number[]=[];
	departmentCount: number[]=[];
	cityCount: number[]=[];

	@Output() handleChange = new EventEmitter<object>();

	change(name: string, value: string, selected: boolean) { // Active filter value change handler
		this.handleChange.emit({ name, value: selected ? value : '' });
	}

	ngOnChanges() { // Getting a list of filters and the number of found values
		this.genderList = Array.from(new Set(this.userList.map(user => user.gender)));
		this.departmentList = Array.from(new Set(this.userList.map(user => user.department)));
		this.cityList = Array.from(new Set(this.userList.map(user => user.address.city)));
		this.genderCount = this.genderList.map(gender => this.userList.filter(user => user.gender === gender).length);
		this.departmentCount = this.departmentList.map(department => this.userList.filter(user => user.department === department).length);
		this.cityCount = this.cityList.map(city => this.userList.filter(user => user.address.city === city).length);
	}
}