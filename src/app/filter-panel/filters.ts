export interface Filters {
	gender: string,
	department: string,
	city: string,
}